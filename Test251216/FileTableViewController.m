//
//  FileTableViewController.m
//  Test251216
//
//  Created by Roman Kazmirchuk on 25.12.16.
//  Copyright © 2016 Roman Kazmirchuk. All rights reserved.
//

#import "FileTableViewController.h"
#import "FileTableViewCell.h"
#import "FileModel.h"
#import "RKConstants.h"

#import <BGTableViewRowActionWithImage.h>

@interface FileTableViewController ()

@property (strong, nonatomic) NSArray<FileModel *> *files;

@end

@implementation FileTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self filesSetting];
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - Setting

- (void)filesSetting {
    
    FileModel *folderFile   = [[FileModel alloc]
                                   init];
    folderFile.filename     = @"Family Shared";
    folderFile.isFolder     = YES;
    folderFile.modDate      = [NSDate dateWithTimeIntervalSinceNow:-24 * 60 * 60];
    folderFile.fileType     = FileTypeOther;
    folderFile.isOrange     = NO;
    folderFile.isBlue       = NO;
    
    FileModel *customFile   = [[FileModel alloc]
                               init];
    customFile.filename     = @"WorkPowerpoint.pptx";
    customFile.isFolder     = NO;
    customFile.modDate      = [NSDate dateWithTimeIntervalSinceNow:-10 * 24 * 60 * 60];
    customFile.fileType     = FileTypeOther;
    customFile.isOrange     = YES;
    customFile.isBlue       = NO;
    
    FileModel *photoFile    = [[FileModel alloc]
                               init];
    photoFile.filename      = @"DSC119.jpg";
    photoFile.isFolder      = NO;
    photoFile.modDate       = [NSDate dateWithTimeIntervalSinceNow:-45 * 24 * 60 * 60];
    photoFile.fileType      = FileTypeImage;
    photoFile.isOrange      = YES;
    photoFile.isBlue        = YES;
    
    self.files              = @[folderFile,
                                customFile,
                                photoFile];

}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 70;

}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *likeAction    = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDefault
                                                                                      title:@"      "
                                                                            backgroundColor:[UIColor whiteColor]
                                                                                      image:[UIImage imageNamed:@"star"]
                                                                              forCellHeight:70
                                                                             andFittedWidth:50
                                            handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                                                
                                                NSLog(@"Like %ld",
                                                      indexPath.row);
                                                
                                            }];
    
    UITableViewRowAction *linkAction    = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDefault
                                                                                      title:@"      "
                                                                            backgroundColor:[UIColor whiteColor]
                                                                                      image:[UIImage imageNamed:@"link"]
                                                                              forCellHeight:70
                                                                             andFittedWidth:50
                                            handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                                                
                                                NSLog(@"Link %ld",
                                                      indexPath.row);
                                                
                                            }];
    
    UITableViewRowAction *deleteAction  = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDefault
                                                                                      title:@"      "
                                                                            backgroundColor:[UIColor whiteColor]
                                                                                      image:[UIImage imageNamed:@"delete"]
                                                                              forCellHeight:70
                                                                             andFittedWidth:50
                                            handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                                                
                                                NSLog(@"Delete %ld",
                                                      indexPath.row);
                                                
                                            }];

    return @[deleteAction,
             linkAction,
             likeAction];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(FileTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {

    FileModel *file = [self.files objectAtIndex:(indexPath.row % [self.files count])];
    
    if (file.isBlue) {
    
        cell.colorBarUpView.backgroundColor     = [RKConstants fileCellBlueColor];
        cell.colorBarDownView.backgroundColor   = file.isOrange ? [RKConstants fileCellOrangeColor] : [RKConstants fileCellBlueColor];
    
    } else if (file.isOrange) {
    
        cell.colorBarUpView.backgroundColor     = [RKConstants fileCellOrangeColor];
        cell.colorBarDownView.backgroundColor   = [RKConstants fileCellOrangeColor];

    } else {
    
        cell.colorBarUpView.backgroundColor     = [UIColor clearColor];
        cell.colorBarDownView.backgroundColor   = [UIColor clearColor];
    
    }
    
    cell.fileNameLabel.text = file.filename;
    if (file.isFolder) {
    
        cell.fileImageView.image    = [UIImage imageNamed:@"folder"];
    
    } else if (file.fileType == FileTypeImage) {
    
        cell.fileImageView.image    = [UIImage imageNamed:@"photo"];
    
    } else {
    
        cell.fileImageView.image    = [UIImage imageNamed:@"file"];
    
    }
    
    NSDateFormatter *dateFormatter  = [[NSDateFormatter alloc]
                                       init];
    [dateFormatter setDateFormat:@"MMMM d, YYYY"];
    NSString *dateString            = [[dateFormatter stringFromDate:file.modDate]
                                       capitalizedString];
    cell.modDateLabel.text          = [NSString stringWithFormat:@"modified %@",
                                       dateString];

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    FileModel *file = [self.files objectAtIndex:(indexPath.row % [self.files count])];
    if (file.isFolder) {
    
        FileTableViewController *fileVC = [[UIStoryboard storyboardWithName:@"Main"
                                                                    bundle:nil]
                                           instantiateViewControllerWithIdentifier:@"FileVC"];
        [self.navigationController pushViewController:fileVC
                                             animated:YES];
        
    
    }

}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 200;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    return [tableView dequeueReusableCellWithIdentifier:@"FileCell"];

}



@end
