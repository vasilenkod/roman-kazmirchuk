//
//  main.m
//  Test251216
//
//  Created by Roman Kazmirchuk on 25.12.16.
//  Copyright © 2016 Roman Kazmirchuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
